## Description

Implement Friend List using RxSwift with MVVM pattern.

## Libraries

- [Alamofire](https://github.com/Alamofire/Alamofire)
- [RxSwift](https://github.com/ReactiveX/RxSwift)
- [IQKeyboardManagerSwift](https://github.com/hackiftekhar/IQKeyboardManager)
