//
//  FriendListCell.swift
//  CathayInterivew
//
//  Created by Joey Hsieh on 2023/2/24.
//

import UIKit

class FriendListCell: UITableViewCell {
    @IBOutlet private var starImageView: UIImageView!
    @IBOutlet private var avatarImageView: UIImageView!
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var moreView: UIView!
    @IBOutlet private var invitedView: UIView!
    @IBOutlet private var invitedLabel: UILabel!
    @IBOutlet private var transferView: UIView!
    @IBOutlet private var transferLabel: UILabel!
    @IBOutlet private var dividingView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func configure(with friend: Friend) {
        starImageView.isHidden = !friend.isTop
        nameLabel.text = friend.name
        moreView.isHidden = friend.status != .accepted
        invitedView.isHidden = friend.status != .invited
    }
}

private extension FriendListCell {
    func setupUI() {
        selectionStyle = .none
        
        avatarImageView.layer.cornerRadius = avatarImageView.bounds.width / 2
        
        nameLabel.textColor = .textBlack
        
        invitedView.layer.borderWidth = 1
        invitedView.layer.borderColor = UIColor.borderLightGray.cgColor
        invitedView.layer.cornerRadius = 3
        
        invitedLabel.text = "邀請中"
        invitedLabel.textColor = .textGray
        
        transferView.layer.borderWidth = 1
        transferView.layer.borderColor = UIColor.accentPink.cgColor
        transferView.layer.cornerRadius = 3
        transferLabel.text = "轉帳"
        transferLabel.textColor = .accentPink
        
        dividingView.backgroundColor = .dividingLine
    }
}
