//
//  InvtationsCell.swift
//  CathayInterivew
//
//  Created by Joey Hsieh on 2023/2/24.
//

import UIKit

class InvtationsCell: UITableViewCell {
    @IBOutlet private var bgView: UIView!
    @IBOutlet private var avatarImageView: UIImageView!
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func configure(with Friend: Friend) {
        nameLabel.text = Friend.name
    }
}

private extension InvtationsCell {
    func setupUI() {
        selectionStyle = .none
        
        bgView.layer.cornerRadius = 8
        
        bgView.layer.shadowColor = UIColor.black.cgColor
        bgView.layer.shadowOffset = .init(width: 0, height: 4)
        bgView.layer.shadowRadius = 5
        bgView.layer.shadowOpacity = 0.08
        
        avatarImageView.layer.cornerRadius = avatarImageView.bounds.width / 2
        
        nameLabel.textColor = .textBlack
        
        messageLabel.text = "邀請你成為好友：）"
        messageLabel.textColor = .textGray
    }
}
