//
//  RequestError.swift
//  CathayInterivew
//
//  Created by Joey Hsieh on 2023/2/23.
//

import Foundation

enum RequestError: Error {
    case badRequest(Error)
    case decodingError
}
