//
//  WebService.swift
//  CathayInterivew
//
//  Created by Joey Hsieh on 2023/2/23.
//

import Alamofire
import Foundation

protocol WebService {
    var baseUrl: String { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var headers: HTTPHeaders? { get }
    var queryItems: [URLQueryItem]? { get }
    var parameters: Parameters? { get }
}

extension WebService {
    var baseUrl: String { "https://dimanyen.github.io" }
    var queryItems: [URLQueryItem]? { nil }
}
