//
//  Service.swift
//  CathayInterivew
//
//  Created by Joey Hsieh on 2023/2/23.
//

import Alamofire
import Foundation

enum Service {
    /// 使用者資料
    case man
    /// 好友列表1
    case friend1
    /// 好友列表2
    case friend2
    /// 好友列表含邀請列表
    case friend3
    /// 無資料邀請/好友列表
    case friend4
}

extension Service: WebService {
    var path: String {
        switch self {
        case .man:
            return "/man.json"
        case .friend1:
            return "/friend1.json"
        case .friend2:
            return "/friend2.json"
        case .friend3:
            return "/friend3.json"
        case .friend4:
            return "/friend4.json"
        }
    }

    var method: HTTPMethod { .get }

    var headers: HTTPHeaders? { nil }

    var parameters: Parameters? { nil }
}
