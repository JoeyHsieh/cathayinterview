//
//  HomeViewModel.swift
//  CathayInterivew
//
//  Created by Joey Hsieh on 2023/2/23.
//

import Foundation
import RxCocoa
import RxSwift

class HomeViewModel {
    struct Info {
        var mans: [Man]
        var friends: [Friend]
    }
    
    private let repository: Repository
    
    private let _infoData = BehaviorRelay<Info?>(value: nil)
    var infoData: Observable<Info?> { _infoData.asObservable() }
    
    private let _isLoadStatus = PublishSubject<Bool>()
    var isLoadStatus: Observable<Bool> {_isLoadStatus.asObserver()}
    
    private let bag = DisposeBag()
    
    init(repository: Repository = Repository()) {
        self.repository = repository
    }
}

extension HomeViewModel {
    func getNoFriendsInfo() {
        _isLoadStatus.onNext(true)
        Observable
            .zip(repository.man(), repository.friend4())
            .map { mans, friends -> Info? in
                guard let mans = mans,
                      let friends = friends else { return nil }
                return Info(mans: mans, friends: friends)
            }
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self._isLoadStatus.onNext(false)
                self._infoData.accept($0)
            })
            .disposed(by: bag)
    }
    
    func getFriendListInfo() {
        _isLoadStatus.onNext(true)
        Observable
            .zip(repository.man(), repository.friend1(), repository.friend2())
            .map { mans, friend1s, friend2s -> Info? in
                guard let mans = mans,
                      let friend1s = friend1s,
                      let friend2s = friend2s else { return nil }
                
                var friends: [Friend] = []
                
                let friend12s = friend1s + friend2s
                
                friend12s.forEach { friend in
                    let target = friend12s
                        .filter { $0.fid == friend.fid }
                        .sorted {
                            guard let updateDate0 = $0.updateDate,
                                  let updateDate1 = $1.updateDate else { return false }
                            return updateDate0 > updateDate1
                        }
                        .first
                    
                    guard let target = target else { return }
                    
                    if friends.filter({ $0.fid == target.fid }).isEmpty {
                        friends.append(target)
                    }
                }
                
                return Info(mans: mans, friends: friends)
            }
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self._isLoadStatus.onNext(false)
                self._infoData.accept($0)
            })
            .disposed(by: bag)
    }
    
    func getWithInvitationsInfo() {
        _isLoadStatus.onNext(true)
        Observable
            .zip(repository.man(), repository.friend3())
            .map { mans, friends -> Info? in
                guard let mans = mans,
                      let friends = friends else { return nil }
                return Info(mans: mans, friends: friends)
            }
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self._isLoadStatus.onNext(false)
                self._infoData.accept($0)
            })
            .disposed(by: bag)
    }
}
