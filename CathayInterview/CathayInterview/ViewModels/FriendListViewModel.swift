//
//  FriendListViewModel.swift
//  CathayInterivew
//
//  Created by Joey Hsieh on 2023/2/23.
//

import Foundation
import RxCocoa
import RxSwift

class FriendListViewModel {
    typealias Info = HomeViewModel.Info

    private let infoData: Info?

    private let _manData = PublishSubject<Man?>()
    var manData: Driver<Man?> { _manData.asDriver(onErrorJustReturn: nil) }

    private let _invitationsData = PublishSubject<[Friend]>()
    var invitationsData: Driver<[Friend]> { _invitationsData.asDriver(onErrorJustReturn: []) }

    private let _friendsData = PublishSubject<[Friend]>()
    var friendsData: Driver<[Friend]> { _friendsData.asDriver(onErrorJustReturn: []) }
    
    private let _pendingData = PublishSubject<Int>()
    var pendingData: Driver<Int> {_pendingData.asDriver(onErrorJustReturn: 0)}

    private let _isNoFriendsFlag = PublishSubject<Bool>()
    var isNoFriendsFlag: Driver<Bool> { _isNoFriendsFlag.asDriver(onErrorJustReturn: false) }

    var searchTextData = PublishSubject<String>()

    private let bag = DisposeBag()

    init(infoData: Info?) {
        self.infoData = infoData
    }

    func start() {
        if let infoData = infoData {
            _manData.onNext(infoData.mans.first)
            _invitationsData.onNext(infoData.friends.filter { $0.status == .pending })
            _friendsData.onNext(infoData.friends.filter { $0.status != .pending })
            _isNoFriendsFlag.onNext(infoData.friends.isEmpty)
            _pendingData.onNext(infoData.friends.filter {$0.status == .invited}.count)
        }
        bindRx()
    }
}

private extension FriendListViewModel {
    func bindRx() {
        searchTextData
            .subscribe(onNext: { [weak self] text in
                guard let self = self,
                      let infoData = self.infoData else { return }
                guard !text.isEmpty else {
                    let friendList = infoData.friends.filter { $0.status != .pending }
                    self._friendsData.onNext(friendList)
                    return
                }

                let friendList = infoData.friends.filter { $0.status != .pending && $0.name.contains(text) }
                self._friendsData.onNext(friendList)
            })
            .disposed(by: bag)
    }
}
