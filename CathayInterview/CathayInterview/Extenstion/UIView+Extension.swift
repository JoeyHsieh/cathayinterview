//
//  UIView+Extension.swift
//  CathayInterivew
//
//  Created by Joey Hsieh on 2023/2/23.
//

import UIKit

extension UIView {
    static var name: String { return"\(self)" }

    func applyGradient(colors: [UIColor?]) -> CAGradientLayer {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colors.compactMap { $0?.cgColor }
        gradient.startPoint = CGPoint(x: 0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1, y: 0.5)
        self.layer.insertSublayer(gradient, at: 0)
        return gradient
    }
}

extension UIViewController {
    static var name: String { return "\(self)" }

    static let indicatorTag = 999

    func startLoading() {
        guard view.viewWithTag(UIViewController.indicatorTag) == nil else { return }
        let indicator: UIActivityIndicatorView
        if #available(iOS 13.0, *) {
            indicator = UIActivityIndicatorView(style: .large)
        } else {
            indicator = UIActivityIndicatorView(style: .gray)
        }
        indicator.center = view.center
        indicator.tag = UIViewController.indicatorTag
        view.addSubview(indicator)
        indicator.startAnimating()
    }

    func stopLoading() {
        guard let indicator = view.viewWithTag(UIViewController.indicatorTag) as? UIActivityIndicatorView else { return }
        indicator.stopAnimating()
        indicator.removeFromSuperview()
    }
}
