//
//  String+Extension.swift
//  CathayInterivew
//
//  Created by Joey Hsieh on 2023/2/23.
//

import UIKit

extension String {
    var boolValue: Bool {
        self == "1" ? true : false
    }

    var dateValue: Date? {
        let formatter = DateFormatter()
        formatter.timeZone = .init(identifier: "GMT")
        switch PunctuationMark(with: self) {
        case .slash:
            formatter.dateFormat = "yyyy/MM/dd"
        case .none:
            formatter.dateFormat = "yyyyMMdd"
        }
        return formatter.date(from: self)
    }

    var placeholder: NSAttributedString {
        .init(
            string: self,
            attributes: [
                .font: UIFont.systemFont(ofSize: 14),
                .foregroundColor: UIColor.textGray
            ]
        )
    }
}

enum PunctuationMark {
    case slash
    case none

    init(with timeString: String) {
        switch true {
        case timeString.contains("/"):
            self = .slash
        default:
            self = .none
        }
    }
}
