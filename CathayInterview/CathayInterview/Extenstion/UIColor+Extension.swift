//
//  UIColor+Extension.swift
//  CathayInterivew
//
//  Created by Joey Hsieh on 2023/2/23.
//

import UIKit

extension UIColor {
    // Accent
    static let accentPink = UIColor(named: "EC008E") ?? rgba(red: 236, green: 0, blue: 140)
    static let accentLightPink = UIColor(named: "F9B2DB") ?? rgba(red: 249, green: 178, blue: 220)

    // Background
    static let backgroundWhite = UIColor(named: "FCFCFC") ?? rgba(red: 252, green: 252, blue: 252)
    static let backgroundGray = UIColor(named: "8E8E8E92") ?? rgba(red: 142, green: 142, blue: 147, alpha: 0.12)

    // Border
    static let borderLightGray = UIColor(named: "C9C9C9") ?? rgba(red: 201, green: 201, blue: 201)

    // Text
    static let textBlack = UIColor(named: "474747") ?? rgba(red: 71, green: 71, blue: 71)
    static let textGray = UIColor(named: "999999") ?? rgba(red: 153, green: 153, blue: 153)
    static let textWhite = UIColor(named: "FFFFFF") ?? rgba(red: 255, green: 255, blue: 255)

    // Dividing Line
    static let dividingLine = UIColor(named: "E4E4E4") ?? rgba(red: 228, green: 228, blue: 228)

    // Shadow
    static let shadowGreen = UIColor(named: "79C41B") ?? rgba(red: 121, green: 196, blue: 27)

    // Button
    static let buttonGreen = UIColor(named: "56B30B") ?? rgba(red: 86, green: 179, blue: 11)
    static let buttonLightGreen = UIColor(named: "A6CC42") ?? rgba(red: 166, green: 204, blue: 66)
}

extension UIColor {
    static func rgba(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat = 1.0) -> UIColor {
        return .init(red: red / 255, green: green / 255, blue: blue / 255, alpha: alpha)
    }
}
