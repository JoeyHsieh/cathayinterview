//
//  Repository.swift
//  CathayInterivew
//
//  Created by Joey Hsieh on 2023/2/23.
//

import Foundation

import RxSwift

protocol RepositoryProtocol {
    func man() -> Observable<[Man]?>
    func friend1() -> Observable<[Friend]?>
    func friend2() -> Observable<[Friend]?>
    func friend3() -> Observable<[Friend]?>
    func friend4() -> Observable<[Friend]?>
}

final class Repository: RepositoryProtocol {
    private let remoteDataSource: DataSource

    init(remoteDataSource: DataSource = RemoteDataSource()) {
        self.remoteDataSource = remoteDataSource
    }

    func man() -> Observable<[Man]?> {
        remoteDataSource.man()
    }

    func friend1() -> Observable<[Friend]?> {
        remoteDataSource.friend1()
    }

    func friend2() -> Observable<[Friend]?> {
        remoteDataSource.friend2()
    }

    func friend3() -> Observable<[Friend]?> {
        remoteDataSource.friend3()
    }

    func friend4() -> Observable<[Friend]?> {
        remoteDataSource.friend4()
    }
}
