//
//  FriendListViewController.swift
//  CathayInterivew
//
//  Created by Joey Hsieh on 2023/2/23.
//

import RxCocoa
import RxSwift
import UIKit

class FriendListViewController: UIViewController {
    @IBOutlet private var manView: UIView!
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var idLabel: UILabel!
    @IBOutlet private var idNotifyView: UIView!
    
    @IBOutlet private var noFriendView: UIView!
    @IBOutlet private var noFriendTitleLabel: UILabel!
    @IBOutlet private var noFriendMessageLabel: UILabel!
    @IBOutlet private var noFriendButtonView: UIView!
    @IBOutlet private var noFriendButton: UIButton!
    @IBOutlet private var setIdTitleLabel: UILabel!
    @IBOutlet private var setIdMessageLabel: UILabel!
    
    @IBOutlet private var friendListStackView: UIStackView!
    
    @IBOutlet private var invitationView: UIView!
    @IBOutlet private var invitationListTableView: UITableView!
    
    @IBOutlet private var segmentControl: UISegmentedControl!
    @IBOutlet private var frinedNotifyView: UIView!
    @IBOutlet private var frinedNotifyLabel: UILabel!
    @IBOutlet private var chatNotifyView: UIView!
    @IBOutlet private var chatNotifyLabel: UILabel!
    @IBOutlet private var segmentSelectedBoardView: UIView!
    
    @IBOutlet private var searchBgView: UIView!
    @IBOutlet private var searchTextField: UITextField!
    
    @IBOutlet private var friendListTableView: UITableView!
    
    var viewModel: FriendListViewModel?
    
    private let bag = DisposeBag()

    init(with viewModel: FriendListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: FriendListViewController.name, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bindRx()
        viewModel?.start()
    }
}

private extension FriendListViewController {
    func setupUI() {
        navigationController?.navigationBar.backgroundColor = .backgroundWhite
        manView.backgroundColor = .backgroundWhite
        
        nameLabel.textColor = .textBlack
        
        idLabel.textColor = .textBlack
        
        idNotifyView.backgroundColor = .accentPink
        idNotifyView.layer.cornerRadius = 5
        
        noFriendTitleLabel.text = "就從加好友開始吧：）"
        noFriendTitleLabel.textColor = .textBlack
        
        noFriendMessageLabel.numberOfLines = 0
        noFriendMessageLabel.text = "與好友們一起用 KOKO 聊起來！\n還能互相收付款、發紅包喔：）"
        noFriendMessageLabel.textAlignment = .center
        noFriendMessageLabel.textColor = .textGray
        
        noFriendButtonView.layer.cornerRadius = 20
        noFriendButtonView.layer.shadowRadius = 8
        noFriendButtonView.layer.shadowOffset = CGSizeMake(0, 4)
        noFriendButtonView.layer.shadowColor = UIColor.shadowGreen.cgColor
        
        noFriendButton.clipsToBounds = true
        _ = noFriendButton.applyGradient(colors: [.buttonGreen, .buttonLightGreen])
        noFriendButton.layer.cornerRadius = 20
        noFriendButton.tintColor = .textWhite
        noFriendButton.setTitle("加好友", for: .normal)
        
        setIdTitleLabel.text = "幫助好友更快找到你？"
        setIdTitleLabel.textColor = .textGray
        
        setIdMessageLabel.textColor = .accentPink
        let underlineAttribute: [NSAttributedString.Key: Any] =
            [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue,
             NSAttributedString.Key.underlineColor: UIColor.accentPink]
        let underlineAttributedString = NSAttributedString(string: "設定 KOKO ID", attributes: underlineAttribute)
        setIdMessageLabel.attributedText = underlineAttributedString
        
        invitationListTableView.contentInset = .init(top: 10, left: 0, bottom: 0, right: 0)
        invitationListTableView.register(UINib(nibName: InvtationsCell.name, bundle: nil),
                                         forCellReuseIdentifier: InvtationsCell.name)
        invitationListTableView.separatorStyle = .none
        invitationListTableView.rx.setDelegate(self).disposed(by: bag)
        
        segmentControl.setBackgroundImage(UIImage(), for: .normal, barMetrics: .default)
        segmentControl.setDividerImage(UIImage(), forLeftSegmentState: .normal,
                                       rightSegmentState: .normal, barMetrics: .default)
        segmentControl.setTitle("好友", forSegmentAt: 0)
        segmentControl.setWidth(60, forSegmentAt: 0)
        segmentControl.setTitle("聊天", forSegmentAt: 1)
        segmentControl.setWidth(60, forSegmentAt: 1)
        
        frinedNotifyView.clipsToBounds = true
        frinedNotifyView.layer.cornerRadius = 9
        frinedNotifyView.backgroundColor = .accentLightPink
        frinedNotifyLabel.textColor = .white
        
        chatNotifyView.clipsToBounds = true
        chatNotifyView.layer.cornerRadius = 9
        chatNotifyView.backgroundColor = .accentLightPink
        chatNotifyLabel.textColor = .white
        chatNotifyLabel.text = "99+"
        
        segmentSelectedBoardView.clipsToBounds = true
        segmentSelectedBoardView.backgroundColor = .accentPink
        segmentSelectedBoardView.layer.cornerRadius = 2
        
        searchBgView.backgroundColor = .backgroundGray
        searchBgView.layer.cornerRadius = 10
        
        searchTextField.attributedPlaceholder = "想轉一筆給誰呢？".placeholder
        searchTextField.tintColor = .accentPink
        
        friendListTableView.register(UINib(nibName: FriendListCell.name, bundle: nil),
                                     forCellReuseIdentifier: FriendListCell.name)
        friendListTableView.separatorStyle = .none
        friendListTableView.rx.setDelegate(self).disposed(by: bag)
    }
    
    func bindRx() {
        guard let viewModel = viewModel else { return }
        
        viewModel.isNoFriendsFlag
            .drive(onNext: { [weak self] isNoFriends in
                self?.noFriendView.isHidden = !isNoFriends
                self?.friendListStackView.isHidden = isNoFriends
            })
            .disposed(by: bag)
        
        viewModel.manData
            .drive(onNext: { [weak self] man in
                guard let self = self else { return }
                self.nameLabel.text = man?.name
                if let id = man?.kokoid {
                    self.idLabel.text = "KOKO ID: \(id)"
                    self.idNotifyView.isHidden = true
                } else {
                    self.idLabel.text = "設定 KOKO ID"
                    self.idNotifyView.isHidden = false
                }
            })
            .disposed(by: bag)
        
        viewModel.invitationsData
            .drive(onNext: { [weak self] friends in
                self?.invitationView.isHidden = friends.isEmpty
            })
            .disposed(by: bag)
        
        viewModel.invitationsData
            .asObservable()
            .bind(to: invitationListTableView.rx.items(cellIdentifier: InvtationsCell.name,
                                                       cellType: InvtationsCell.self)) {
                _, friend, cell in
                cell.configure(with: friend)
            }
            .disposed(by: bag)
        
        viewModel.pendingData
            .drive(onNext: { [weak self] pendingCount in
                guard let self = self else { return }
                self.frinedNotifyView.isHidden = pendingCount == 0
                self.frinedNotifyLabel.text = "\(pendingCount)"
            })
            .disposed(by: bag)
        
        viewModel.friendsData
            .asObservable()
            .bind(to: friendListTableView.rx.items(cellIdentifier: FriendListCell.name,
                                                   cellType: FriendListCell.self)) {
                _, friend, cell in
                cell.configure(with: friend)
            }
            .disposed(by: bag)
        
        searchTextField.rx.text
            .orEmpty
            .subscribe(onNext: { [weak self] text in
                guard let self = self,
                      let viewModel = self.viewModel else { return }
                viewModel.searchTextData.onNext(text)
            })
            .disposed(by: bag)
    }
}

extension FriendListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch tableView {
        case invitationListTableView:
            return 80
        case friendListTableView:
            return 60
        default:
            return 0
        }
    }
}
