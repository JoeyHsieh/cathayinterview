//
//  HomeViewController.swift
//  CathayInterivew
//
//  Created by Joey Hsieh on 2023/2/23.
//

import RxCocoa
import RxSwift
import UIKit

class HomeViewController: UIViewController {
    @IBOutlet var noFriendButton: UIButton!
    @IBOutlet var friendListButton: UIButton!
    @IBOutlet var withoutInvitationsButton: UIButton!

    private let viewModel = HomeViewModel()

    private let bag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        bindRx()
    }
}

private extension HomeViewController {
    func bindRx() {
        viewModel.isLoadStatus
            .subscribe(onNext: { [weak self] status in
                if status {
                    self?.startLoading()
                } else {
                    self?.stopLoading()
                }
            }).disposed(by: bag)
        
        viewModel.infoData
            .subscribe(onNext: { [weak self] info in
                guard let self = self, info != nil else { return }
                let vc = FriendListViewController(with: .init(infoData: info))
                self.navigationController?.pushViewController(vc, animated: true)
            })
            .disposed(by: bag)

        noFriendButton.rx.tap
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.viewModel.getNoFriendsInfo()
            })
            .disposed(by: bag)

        friendListButton.rx.tap
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.viewModel.getFriendListInfo()

            })
            .disposed(by: bag)

        withoutInvitationsButton.rx.tap
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.viewModel.getWithInvitationsInfo()
            })
            .disposed(by: bag)
    }
}
